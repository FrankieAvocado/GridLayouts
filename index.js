var express = require("express");
var environment = process.env.ENVIRONMENT;
var app = express();

var port = process.env.PORT || 8080;
app.use(express.static("wwwroot"));

app.listen(port);
console.log("The audience is listening...on port: " + port);